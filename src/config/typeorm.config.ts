import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import { Province } from '../provinces/entities/province.entitiy';
import { ProvinceCap } from '../provinces/entities/province.cap.entity';
import tns from 'tns'
const fs = require('fs');

const contents = fs.readFileSync(process.env.TNS_ADMIN, 'utf-8');
const ast = tns(contents)

//Configurazione di TypeORM
export const typeOrmConfig: TypeOrmModuleOptions = {
  type: "oracle", //Tipologia di connessione, nel nostro caso ORACLE
  host: ast[process.env.SERVICE_NAME].DESCRIPTION.ADDRESS.HOST,
  port: ast[process.env.SERVICE_NAME].DESCRIPTION.ADDRESS.PORT,
  username: process.env.USER, //Username
  password: process.env.PASSWORD, //Password
  "entities": [
    Province, ProvinceCap
  ],
  sid: ast[process.env.SERVICE_NAME].DESCRIPTION.CONNECT_DATA.SERVICE_NAME,
  synchronize: false,
};