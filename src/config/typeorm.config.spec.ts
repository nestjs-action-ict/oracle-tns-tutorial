const fs = require('fs');
import tns from 'tns';


describe('typeorm.config', () => {

    const OLD_ENV = process.env;

    beforeEach(() => {
        jest.resetModules() // Most important - it clears the cache
        process.env = { ...OLD_ENV }; // Make a copy
        process.env.SERVICE_NAME = 'MYSID';
        process.env.USER = 'mockedUser';
        process.env.PASSWORD = 'mockedPassword';
        process.env.TNS_ADMIN =  './test/test.tnsnames.ora';
    });

    afterAll(() => {
        process.env = OLD_ENV; // Restore old environment
    });

    it('should be defined', () => {
        const typeOrmConfig = require('./typeorm.config');
        expect(typeOrmConfig).toBeDefined();
    });

    it('should contains oracle props from tnsnames', () => {
        const typeOrmConfig = require('./typeorm.config');
        expect(typeOrmConfig.typeOrmConfig.type).toBe('oracle');
        expect(typeOrmConfig.typeOrmConfig.host).toBe('mydnshostname');
        expect(typeOrmConfig.typeOrmConfig.port).toBe('1521');
        expect(typeOrmConfig.typeOrmConfig.sid).toBe('MYSID');
    });

    it('should contains oracle props from process.env', () => {
        const typeOrmConfig = require('./typeorm.config');
        expect(typeOrmConfig.typeOrmConfig.username).toBe('mockedUser');
        expect(typeOrmConfig.typeOrmConfig.password).toBe('mockedPassword');
    });
});
