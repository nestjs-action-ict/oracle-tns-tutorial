import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Response<T> {
  "data": any,
  "errors": [
    {
      "code": "string",
      "description": "string",
      "severity": "string",
      "type": "string"
    }
  ],
  "info": [
    {
      "code": "string",
      "description": "string",
      "severity": "string",
      "type": "string"
    }
  ],
  "warnings": [
    {
      "code": "string",
      "description": "string",
      "severity": "string",
      "type": "string"
    }
  ]
}

@Injectable()
export class TransformInterceptor<T> implements NestInterceptor<T, Response<T>> {
  intercept(context: ExecutionContext, next: CallHandler): Observable<Response<T>> {


    return next.handle().pipe(map(data => {
      return {data: Array.isArray(data) ? data : [data], errors: [null], info: [null], warnings: [null]}
    }));
  }
}
