import {Test, TestingModule} from "@nestjs/testing";
import {TransformInterceptor} from "./transform.interceptor";
import {Observable, of} from "rxjs";

describe('transform.interceptor', () => {


  describe('TransformInterceptor', ()=> {
    let interceptor;

    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        providers: [
          TransformInterceptor

        ],
      }).compile();
      interceptor = module.get<TransformInterceptor<any>>(TransformInterceptor);
    })

    const executionContext = {};

    const myData = {data: 'value'}


    const next = {
      handle: jest.fn(() => of(myData)),
    }



    it('should be defined', () => {
      expect(interceptor).toBeDefined();
    });

    it('should transform correctly data', () => {
      const responseInterceptor: Observable<any> = interceptor.intercept(executionContext, next);

      responseInterceptor.subscribe({
        next: value => {
          expect(value).toStrictEqual({
            data: [ myData ],
            errors: [ null ],
            info: [ null ],
            warnings: [ null ]
          })
        },
      });
    });


  });
});
