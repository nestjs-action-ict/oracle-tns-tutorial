export const common = {
  exists: (value: any): boolean => !(typeof value === 'undefined' || value === null)
};

