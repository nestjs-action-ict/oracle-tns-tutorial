import {GlobalExceptionFilter, httpErrorParse} from "./global.exception.filter";
import {Test, TestingModule} from "@nestjs/testing";
import {HttpException, ArgumentsHost, UnauthorizedException} from "@nestjs/common";

describe('global.exception.filter', () => {

  describe('httpErrorParse', () => {
    it('should return correct parsed value', ()=>{
      expect(httpErrorParse('myError')).toBe('Generic Error');
      expect(httpErrorParse(404)).toBe('Not Found');
      expect(httpErrorParse(400)).toBe('Bad Request');
      expect(httpErrorParse(500)).toBe('Internal Server Error');
    })
  })

  describe('GlobalExceptionFilter', ()=> {
    let filter;

    beforeEach(async () => {
      const module: TestingModule = await Test.createTestingModule({
        providers: [
          GlobalExceptionFilter
        ],
      }).compile();
      filter = module.get<GlobalExceptionFilter>(GlobalExceptionFilter);
    })

    const mockJson = jest.fn().mockImplementation(o => o);

    const mockStatus = jest.fn().mockImplementation(() => ({
      json: mockJson
    }));

    const mockGetResponse = jest.fn().mockImplementation(() => ({
      status: mockStatus,
      json: mockJson
    }));

    const mockHttpArgumentsHost = jest.fn().mockImplementation(() => ({
      getResponse: mockGetResponse,
      getRequest: jest.fn()
    }));

    const mockArgumentsHost = {
      switchToHttp: mockHttpArgumentsHost,
      getArgByIndex: jest.fn(),
      getArgs: jest.fn(),
      getType: jest.fn(),
      switchToRpc: jest.fn(),
      switchToWs: jest.fn()
    };


    it('should be defined', () => {
      expect(filter).toBeDefined();
    });

    it('should return an error when exception is an HttpException', ()=>{

      const exception = new HttpException('Eccezione HTTP', 401);
      filter.catch(exception, mockArgumentsHost)

      const error = {
        code: exception.getStatus(),
        description: exception.message,
        severity: '01',
        type: httpErrorParse(exception.getStatus()),
        source: null
      }

      const args = {
        data: [],
        info: [],
        warnings: [],
        errors: [error]
      }

      expect(mockJson).toBeCalledWith(args);
    })

    it('should return an exception when exception isn\'t HttpException', ()=>{
      const exception = new UnauthorizedException('unauthorized');
      filter.catch(exception, mockArgumentsHost)
      const args = {
        data: [],
        info: [],
        warnings: [],
        errors: [{
          code: 401,
          description: "unauthorized",
          severity: "01",
          source: null,
          type: "Generic Error",
        }],
      }
      expect(mockJson).toBeCalledWith(args);
    })
  });
});
