import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus, NotFoundException } from '@nestjs/common';


export const httpErrorParse = function(code) {
  switch (code) {
    case HttpStatus.NOT_FOUND: return 'Not Found'
    case HttpStatus.BAD_REQUEST: return 'Bad Request'
    case HttpStatus.INTERNAL_SERVER_ERROR: return 'Internal Server Error'
    default: return 'Generic Error'
  }
}

@Catch()
export class GlobalExceptionFilter implements ExceptionFilter {
  catch(exception: unknown, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    if (exception instanceof HttpException) {
      const error =
        {
          code: exception.getStatus(),
          description: exception.message,
          severity: '01',
          type: httpErrorParse(exception.getStatus()),
          source: null
        }
      response.json({
        data: [],
        info: [],
        warnings: [],
        errors: [error]
      })
    } else {
      response.json({
        data: [],
        info: [],
        warnings: [],
        errors: exception
      });
    }
  }
}
