import { Injectable } from '@nestjs/common';

export type User = any;

@Injectable()
export class UsersService {
  private readonly users: User[];

  constructor() {
    this.users = [
      {
        userId: 1149996,
        username: 'testautomation',
        password: 'secret1'
      },
      {
        userId: 1149997,
        username: 'testautomation2',
        password: 'secret2'
      },
      {
        userId: null,
        username: 'unauthorizedToken',
        password: 'secret3'
      },
    ];
  }

  async findOne(username: string): Promise<User | undefined> {
    return this.users.find(user => user.username === username);
  }
}
