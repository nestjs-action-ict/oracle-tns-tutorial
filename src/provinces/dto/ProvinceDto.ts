import { Province } from '../interfaces/province.interface';

export class ProvinceDto implements Province {

  active: boolean;
  code: string;
  description: string;
  istatCode: string;
  istatProvincialCapital: string;
  regionCode: string;

}
