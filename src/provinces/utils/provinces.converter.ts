import { Injectable } from '@nestjs/common';
import { Province } from '../interfaces/province.interface';
import { ProvinceDto} from '../dto/ProvinceDto';
import { Province as ProvinceEntity } from '../entities/province.entitiy';

@Injectable()
export class ProvincesConverter {

  fromEntity(province: ProvinceEntity): Province {
    const result = new ProvinceDto();
    result.code = province.IPR_PROVINCIA;
    result.description = province.IPR_DESCR;
    result.istatCode = province.IPR_ISTAT;
    result.istatProvincialCapital = province.IPR_ISTAT_CAPOL;
    result.regionCode = province.IPR_REGIONE;
    result.active = province.IPR_ATTIVO === '1';
    return result;
  }

  fromEntities(provinces: ProvinceEntity[]): Province[] {
    const result = [];
    provinces.forEach( province => result.push(this.fromEntity(province)));
    return result;
  }
}


