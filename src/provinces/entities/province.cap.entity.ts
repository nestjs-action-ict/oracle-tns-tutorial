import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';
import { Province } from './province.entitiy';

@Entity("INDPROVINCECAP")
export class ProvinceCap {

  @PrimaryColumn()
 // @ManyToOne( type => Province, province => province.IPR_TARGA)
  IPC_TARGA: string;

  @PrimaryColumn()
  IPC_CAP: string;

  @Column()
  IPC_ATTIVO: string;

  @Column()
  IPC_MULTI_CAP: string;

}
