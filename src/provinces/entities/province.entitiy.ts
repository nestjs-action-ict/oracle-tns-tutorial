import { Entity, PrimaryColumn, Column, OneToMany , JoinColumn} from 'typeorm';
import { ProvinceCap } from './province.cap.entity';

@Entity("INDPROVINCE")
export class Province {
  @PrimaryColumn()
  IPR_PROVINCIA: string;

  @Column()
  IPR_REGIONE: string;

  @Column()
  IPR_TARGA: string;

  @Column()
  IPR_DESCR: string;

  @Column()
  IPR_ISTAT: string;

  @Column()
  IPR_ISTAT_CAPOL: string;

  @Column()
  IPR_ATTIVO: string;


}
