import {Test, TestingModule} from "@nestjs/testing";
import {OnlyActivePipe, PostalCodePipe} from "./provinces.pipe";

describe('province.pipe', () => {


    describe('OnlyActivePipe', ()=> {
        let pipe;

        beforeEach(async () => {
            const module: TestingModule = await Test.createTestingModule({
                providers: [
                    OnlyActivePipe
                ],
            }).compile();
            pipe = module.get<OnlyActivePipe>(OnlyActivePipe);
        })

        it('should be defined', () => {
            expect(pipe).toBeDefined();
        });

        it('parameter onlyActive should be 1 when onlyActive is \'true\'', () => {
            const result = pipe.transform({onlyActive: 'true'})
            expect(result).toStrictEqual({onlyActive: '1'});
        });

        it('parameter onlyActive should be 0 when onlyActive is \'false\'', () => {
            const result = pipe.transform({onlyActive: 'false'})
            expect(result).toStrictEqual({onlyActive: '0'});
        });

        it('should return an exception where onlyActive is not defined', () => {
            try {
                pipe.transform({only: true})
            }
            catch (err) {
                expect(err.getResponse().message).toEqual(["onlyActive can be only false or true"])
            }
        });

        it('should return an exception where onlyActive is different to 0 and 1', () => {
            try {
                pipe.transform({onlyActive: '10'})
            } catch (err) {
                expect(err.getResponse().message).toEqual("onlyActive can be only false or true");
            }
        });
    });


    describe('PostalCodePipe', ()=> {
        let pipe;

        beforeEach(async () => {
            const module: TestingModule = await Test.createTestingModule({
                providers: [
                    PostalCodePipe
                ],
            }).compile();
            pipe = module.get<PostalCodePipe>(PostalCodePipe);
        })

        it('should be defined', () => {
            expect(pipe).toBeDefined();
        });

        it('should throw an exception when postalCode is missing', () => {
            try {
                pipe.transform({onlyActive: 'true'})
            }
            catch (err) {
                expect(err.getResponse().message).toEqual("required Postal Code is Missing");
            }
        });

        it('should throw an exception when postalCode is not numeric', () => {
            try {
                pipe.transform({postalCode: 'AAAAA'})
            }
            catch (err) {
                expect(err.getResponse().message).toEqual("required Postal Code is Invalid");
            }
        });

        it('should throw an exception when postalCode length is <> 5>', () => {
            try {
                pipe.transform({postalCode: '1234'})
            }
            catch (err) {
                expect(err.getResponse().message).toEqual("required Postal Code is Invalid");
            }
        });

        it('should not throw an exception when postalCode is valid', () => {

            const result = pipe.transform({postalCode: '12345'})
            expect(result).toStrictEqual({postalCode: '12345'});
        });
    });
});
