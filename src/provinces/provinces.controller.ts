import { Controller, Get, Param, Query, UseGuards, UseInterceptors, UsePipes } from '@nestjs/common';
import { ProvincesService } from './provinces.service';
import { Province } from './interfaces/province.interface';
import { OnlyActivePipe, PostalCodePipe } from './provinces.pipe';
import { common } from '../common/utils';
import { TransformInterceptor } from '../interceptors/transform.interceptor';
import { JwtAuthGuard } from '../auth/jwt-auth-guard';

@Controller('provinces')
export class ProvincesController {
  constructor(private readonly provincesService: ProvincesService) {}



  @Get()
  @UsePipes(new OnlyActivePipe())
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(TransformInterceptor)
  async findAll(@Query() params): Promise<Province[]> {
    return common.exists(params.onlyActive) ?
      this.provincesService.findAllWihParams(params.onlyActive):
      this.provincesService.findAll();
  }

  @Get('/postalCode/:postalCode')
  @UseGuards(JwtAuthGuard)
  @UsePipes(new PostalCodePipe())
  @UseInterceptors(TransformInterceptor)
  async findByPostalCode(@Param() params) {
    return this.provincesService.findByPostalCode(params.postalCode)

  }
}
