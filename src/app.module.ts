import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeOrmConfig } from './config/typeorm.config';
import { NestFactory } from '@nestjs/core';
import { AuthModule } from './auth/auth.module';
import { ProvincesModule } from './provinces/provinces.module';
import { UsersModule } from './users/users.module';
import { DealerModule } from './dealer/dealer.module';
import { DealerService } from './dealer/dealer.service';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    ProvincesModule,
    AuthModule,
    UsersModule,
    DealerModule
  ],
  providers: [DealerService]
})
export class AppModule {}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}


bootstrap();
