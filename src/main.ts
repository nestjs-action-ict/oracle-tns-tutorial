import { startup } from './startup';

async function bootstrap() {
  await startup();
  require('./app.module');
}

bootstrap();
