import { Module } from '@nestjs/common';
import { DealerConverter } from './utils/dealer.converter';

@Module({
  providers: [DealerConverter],
  exports: [DealerConverter]
})
export class DealerModule {}
