export interface Dealer {
  channel: string;
  principalName: string;
  grantedRoles: Array<string>;
  tvei: string;
  market: string;
  group: string;
  union: string;
  chain: string;
}
