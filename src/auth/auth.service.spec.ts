import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import {JwtService} from "@nestjs/jwt";
import {UsersService} from "../users/users.service";

const mockJwtService = () => ({
  sign: jest.fn().mockImplementation(() => {return 'myToken'})
});
const mockUserService = () => ({
  findOne: jest.fn()
});

describe('AuthService', () => {
  let service: AuthService;
  let userService;
  let jwtService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
          AuthService,
          {provide: UsersService, useFactory: mockUserService},
          {provide: JwtService, useFactory: mockJwtService},

      ],
    }).compile();

    userService = module.get<UsersService>(UsersService);
    jwtService = module.get<JwtService>(JwtService);
    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('validateUser', () => {

    it('Should return null when username is not set', async () => {
      const result = await service.validateUser(null, 'password');
      expect(result).toBeFalsy();
    })

    it('Should call userService', async () => {
      userService.findOne.mockResolvedValue({});
      await service.validateUser('user', 'password');
      expect(userService.findOne).toBeCalled();
    })

    it('Should not return the password we set in the sign', async () => {
      userService.findOne.mockResolvedValue({ name: 'myName', type: 'admin', password: 'myPassword'});
      const result = await service.validateUser('myUsername', 'myPassword');
      expect(result.password).toBeFalsy();
    })

    it('Should return a valid user when username is set properly', async () => {
      userService.findOne.mockResolvedValue({ name: 'myName', type: 'admin', password: 'myPassword'});
      const result = await service.validateUser('myUsername', 'myPassword');
      expect(result).toBeTruthy();
    })
  })

  describe('buildPayload', ()=>{
    it('Should return an object containing sub and username', ()=>{
      const result = service.buildPayload('myUsername', 'myUserId');
      expect(result.sub).toBeDefined();
      expect(result.user_name).toBeDefined();
    })

    it('sub should be the concatenation of username and userId', ()=>{
      const result = service.buildPayload('myUsername', '1234');
      expect(result.sub).toBe('myUsername1234');
    })

    it('user_name should be the concatenation of username and userId', ()=>{
      const result = service.buildPayload('myUsername', '1234');
      expect(result.user_name).toBe('myUsername1234');
    })

    it('tvei should be the username', ()=>{
      const result = service.buildPayload('myUsername', '1234');
      expect(result.tvei).toBe('1234');
    })

  })

  describe('login', ()=>{
    it('jwtService.sign should be called', async ()=>{
      jwtService.sign.mockResolvedValue('myToken');
      await service.login({username: 'myUsername', userId: '1234'})
      expect(jwtService.sign).toBeCalled();
    })

    it('should return a valid access_token', async ()=>{
      const result = await service.login({username: 'myUsername', userId: '1234'})
      expect(result.access_token).toBe('myToken');
    })
  })

});
