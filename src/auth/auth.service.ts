import { Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    if (username === null) {
      return null;
    }
    const user = await this.userService.findOne(username);
    if (user && user.password === pass) {
      const { password, ...result} = user;
      return result;
    }
    return null;
  }

  buildPayload(username: string, userId: string): any {
    return {
      "sub": username + userId,
      "user_name": username + userId,
      "channel": "venditori",
      "realmName": "mga",
      "iss": "https://pan.frontend.findomestic.local",
      "market": "012",
      "tvei": userId,
      "iat": 1562752303,
      "authorities": [
        "FINCARTA_OPERATORE",
        "GIFTCARD_OPERATORE",
        "1001149996",
        "UNICA_DIVERSI_OPERATORE",
        "UNICA_IN_GESTIONE_OPERATORE",
        "UNICA_GIORNALE_OPERATORE",
        "UNICA_VEICOLI_OPERATORE",
        "CQS_COMMERCIALE_OPERATORE",
        "CQS_GESTORE_OPERATORE",
        "UNICA_LEASING_OPERATORE",
        "CQS_FORMLIGHT_OPERATORE",
        "PORTALECQS_NETWORK_OPERATORE",
        "PORTALECQS_NETWORKAVANZATO_OPERATORE",
        "PORTALECQS_NETWORKCOMMEXT_OPERATORE",
        "PORTALECQS_SEGNALANTE_OPERATORE",
        "PORTALECQS_BANCA_OPERATORE",
        "PORTALECQS_BANCAAVANZATO_OPERATORE",
        "DAM_OPERATORE",
        "DRS_OPERATORE",
        "DCM_OPERATORE",
        "DCL_OPERATORE",
        "DFP_OPERATORE"
      ]
    }
  }



  async login(user: any) {
    const payload = this.buildPayload(user.username, user.userId);
    return {
      access_token: this.jwtService.sign(payload)
    }
  }


}
