import { Controller, Get, Post, Request, UseGuards, UseInterceptors } from '@nestjs/common';
import { LocalAuthGuard } from './local-auth-guard';
import { AuthService } from './auth.service';
import { TransformInterceptor } from '../interceptors/transform.interceptor';
import { JwtAuthGuard } from './jwt-auth-guard';

@Controller()

export class AuthController {

  constructor(private authService: AuthService) {
  }

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @UseInterceptors(TransformInterceptor)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }
  @Post('/dossier/modify/ibm')
  async modifyDossier(@Request() req) {
    console.log('Request arrived: ', req);
    return 'OK';
  }

}
